/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import org.apache.commons.math3.util.FastMath;
import org.junit.Test;
import static org.visnow.jlargearrays.FloatingPointAsserts.*;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class LargeArrayStatisticsTest extends LargeArrayTest
{

    public LargeArrayStatisticsTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testMin()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);

        double res = LargeArrayStatistics.min(a);
        double min = a.getDouble(0);
        for (long i = 1; i < a.length(); i++) {
            if (a.getDouble(i) < min) {
                min = a.getDouble(i);
            }
        }
        assertRelativeEquals(min, res);

        a = LargeArrayUtils.createConstant(LargeArrayType.BYTE, 10, (byte) 2);
        res = LargeArrayStatistics.min(a);
        assertRelativeEquals(2.0, res);
    }

    @Test
    public void testMax()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);

        double res = LargeArrayStatistics.max(a);
        double max = a.getDouble(0);
        for (long i = 1; i < a.length(); i++) {
            if (a.getDouble(i) > max) {
                max = a.getDouble(i);
            }
        }
        assertRelativeEquals(max, res);

        a = LargeArrayUtils.createConstant(LargeArrayType.BYTE, 10, (byte) 2);
        res = LargeArrayStatistics.max(a);
        assertRelativeEquals(2.0, res);

    }

    @Test
    public void testSum()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);

        double res = LargeArrayStatistics.sum(a);
        double sum = 0;
        for (long i = 0; i < a.length(); i++) {
            sum += a.getDouble(i);
        }
        assertRelativeEquals(sum, res);

        a = LargeArrayUtils.createConstant(LargeArrayType.BYTE, 10, (byte) 2);
        res = LargeArrayStatistics.sum(a);
        assertRelativeEquals(20.0, res);

    }

    @Test
    public void testSumKahan()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);

        double res = LargeArrayStatistics.sumKahan(a);
        double sum = 0;
        for (long i = 0; i < a.length(); i++) {
            sum += a.getDouble(i);
        }
        assertRelativeEquals(sum, res);

        a = LargeArrayUtils.createConstant(LargeArrayType.BYTE, 10, (byte) 2);
        res = LargeArrayStatistics.sumKahan(a);
        assertRelativeEquals(20.0, res);

    }

    @Test
    public void testAvg()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);

        double res = LargeArrayStatistics.avg(a);
        double sum = 0;
        for (long i = 0; i < a.length(); i++) {
            sum += a.getDouble(i);
        }
        assertRelativeEquals(sum / a.length(), res);

        a = LargeArrayUtils.createConstant(LargeArrayType.BYTE, 10, (byte) 2);
        res = LargeArrayStatistics.avg(a);
        assertRelativeEquals(2.0, res);

    }

    @Test
    public void testAvgKahan()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);

        double res = LargeArrayStatistics.avgKahan(a);
        double sum = 0;
        for (long i = 0; i < a.length(); i++) {
            sum += a.getDouble(i);
        }
        assertRelativeEquals(sum / a.length(), res);

        a = LargeArrayUtils.createConstant(LargeArrayType.BYTE, 10, (byte) 2);
        res = LargeArrayStatistics.avgKahan(a);
        assertRelativeEquals(2.0, res);

    }

    @Test
    public void testStd()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);

        double res = LargeArrayStatistics.std(a);
        double sum = 0;
        double sum2 = 0;
        for (long i = 0; i < a.length(); i++) {
            sum += a.getDouble(i);
            sum2 += (a.getDouble(i) * a.getDouble(i));
        }
        sum /= a.length();
        sum2 /= a.length();
        assertRelativeEquals(FastMath.sqrt(FastMath.max(0, sum2 - sum * sum)), res);

        a = LargeArrayUtils.createConstant(LargeArrayType.BYTE, 10, (byte) 2);
        res = LargeArrayStatistics.std(a);
        assertRelativeEquals(0.0f, res);

    }

    @Test
    public void testStdKahan()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);

        double res = LargeArrayStatistics.stdKahan(a);
        double sum = 0;
        double sum2 = 0;
        for (long i = 0; i < a.length(); i++) {
            sum += a.getDouble(i);
            sum2 += (a.getDouble(i) * a.getDouble(i));
        }
        sum /= a.length();
        sum2 /= a.length();
        assertRelativeEquals(FastMath.sqrt(FastMath.max(0, sum2 - sum * sum)), res);

        a = LargeArrayUtils.createConstant(LargeArrayType.BYTE, 10, (byte) 2);
        res = LargeArrayStatistics.stdKahan(a);
        assertRelativeEquals(0.0, res);
    }
}
