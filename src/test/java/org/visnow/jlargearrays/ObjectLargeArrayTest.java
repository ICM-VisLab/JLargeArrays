/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class ObjectLargeArrayTest extends LargeArrayTest
{

    public ObjectLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testEmptyObjectLargeArray()
    {
        ObjectLargeArray a = new ObjectLargeArray(0);
        assertEquals(0, a.length());
        Throwable e = null;
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e = null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);

        a = new ObjectLargeArray(0, 100, 1, true);
        assertEquals(0, a.length());
        assertTrue(a.isConstant());
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e = null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
    }

    @Test
    public void testObjectLargeArrayConstructor()
    {
        Throwable e = null;
        ObjectLargeArray a = null;
        try {
            a = new ObjectLargeArray(1, LargeArray.DEFAULT_MAX_OBJECT_SIZE, new NonSerializableClass());
        } catch (Throwable ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalArgumentException);
        e = null;
        try {
            a = new ObjectLargeArray(1, LargeArray.DEFAULT_MAX_OBJECT_SIZE, new SerializableClass());
        } catch (Throwable ex) {
            e = ex;
        }
        assertEquals(1, a.length());

        Object[] a1 = new Object[]{new NonSerializableClass(), new SerializableClass()};
        Object[] a2 = new Object[]{new SerializableClass(), new NonSerializableClass()};
        Object[] a3 = new Object[]{new NonSerializableClass(), new NonSerializableClass()};
        Object[] a4 = new Object[]{new SerializableClass(), new SerializableClass()};

        try {
            a = new ObjectLargeArray(a1);
        } catch (Throwable ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalArgumentException);
        e = null;
        try {
            a = new ObjectLargeArray(a2);
        } catch (Throwable ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalArgumentException);
        e = null;
        try {
            a = new ObjectLargeArray(a3);
        } catch (Throwable ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalArgumentException);
        e = null;
        try {
            a = new ObjectLargeArray(a4);
        } catch (Throwable ex) {
            e = ex;
        }
        assertEquals(a4.length, a.length());
    }

    @Test
    public void testObjectLargeArrayEqualsHashCode()
    {
        ObjectLargeArray a = new ObjectLargeArray(10);
        ObjectLargeArray b = new ObjectLargeArray(10);
        a.set(0, 1);
        b.set(0, 1);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.set(0, 2);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
        a.set(0, 3);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testObjectLargeArrayApproximateHashCode()
    {
        ObjectLargeArray a = new ObjectLargeArray(10);
        ObjectLargeArray b = new ObjectLargeArray(10);
        a.set(0, new Double(0));
        b.set(0, new Double(1));
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new ObjectLargeArray(10, LargeArray.DEFAULT_MAX_OBJECT_SIZE, new Double(0), true);
        b = new ObjectLargeArray(10, LargeArray.DEFAULT_MAX_OBJECT_SIZE, new Double(1), true);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testSerialization()
    {
        long size = 1 << 5;
        ObjectLargeArray a = new ObjectLargeArray(size);
        ObjectLargeArray b = null;
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (ObjectLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        for (long i = 0; i < size; i++) {
            a.set(i, new Float(i));
        }
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (ObjectLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        a = new ObjectLargeArray(size, LargeArray.DEFAULT_MAX_OBJECT_SIZE, new Float(12345), true);
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (ObjectLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
    }

    @Test
    public void testObjectLargeArrayConstant()
    {
        ObjectLargeArray a = new ObjectLargeArray(10, LargeArray.DEFAULT_MAX_OBJECT_SIZE, new Float(12345), true);
        assertTrue(a.isConstant());
        assertEquals(12345f, a.get(0));
        assertEquals(12345f, a.get(a.length() - 1));
        a.set(0, new Float(12346));
        assertEquals(12346f, a.get(0));
        assertFalse(a.isConstant());

        a = new ObjectLargeArray(10, LargeArray.DEFAULT_MAX_OBJECT_SIZE, null, false);
        assertFalse(a.isConstant());
        assertEquals(null, a.get(0));

        a = new ObjectLargeArray(10, LargeArray.DEFAULT_MAX_OBJECT_SIZE, null, true);
        assertTrue(a.isConstant());
        assertEquals(null, a.get(0));
    }

    @Test
    public void testObjectLargeArrayGetSet()
    {
        ObjectLargeArray a = new ObjectLargeArray(10, 84);
        long idx = 5;
        Double val1 = 2.0;
        Double val2 = Double.MAX_VALUE;
        a.set(idx, val1);
        assertEquals(val1, a.get(idx));
        a.set(idx, val2);
        assertEquals(val2, a.get(idx));
        assertEquals(val2.getClass(), a.getElementClass());
        Throwable e = null;
        a = new ObjectLargeArray(1);
        try {
            a.set(1, new NonSerializableClass());
        } catch (Throwable ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalArgumentException);
    }

    @Test
    public void testObjectLargeArrayGetSetNative()
    {
        ObjectLargeArray a = new ObjectLargeArray(10, 84);
        if (a.isLarge()) {
            long idx = 5;
            Double val1 = 2.0;
            Double val2 = Double.MAX_VALUE;
            a.setToNative(idx, val1);
            assertEquals(val1, a.getFromNative(idx));
            a.setToNative(idx, val2);
            assertEquals(val2, a.getFromNative(idx));
            assertEquals(val2.getClass(), a.getElementClass());
        }
        Throwable e = null;
        a = new ObjectLargeArray(1);
        if (a.isLarge()) {
            try {
                a.set(1, new NonSerializableClass());
            } catch (Throwable ex) {
                e = ex;
            }
            assertTrue(e instanceof IllegalArgumentException);
        }
    }
}
